#ifndef _CONFIG_H
#define _CONFIG_H

#define BEADS_VERSION "@BEADS_VERSION@"

#define cimg_display 0

#define cimg_use_jpeg 1
#define cimg_use_tiff 1
#define cimg_use_png 1
#define cimg_use_zlib 1


#define cimg_OS @CIMG_OS@

#endif /* _CONFIG_H */
