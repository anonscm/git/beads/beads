#ifndef SPOTDOCUMENT_H_
#define SPOTDOCUMENT_H_

#include <QDebug>
#include <odsstream/calcwriterinterface.h>

#include "detection.h"
#include "spot.h"
#include "config.h"

class spotDocument {
public:
    spotDocument(CalcWriterInterface * p_writer);

    ~spotDocument();

    void set_gel_image_file_name(const QString & filename) {
        _gel_image_file_name = filename;
    }
    const QString & get_gel_image_file_name() const {
        return (_gel_image_file_name);
    }

    virtual void open();
    virtual void close();

    virtual void write_detection(detection & the_detection);

    virtual void write_spot(const spot &);

protected:
    spotDocument();

    QString _gel_image_file_name;

    CalcWriterInterface * _p_writer;
};

#endif /*SPOTDOCUMENT_H_*/
